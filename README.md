My own Dockerfiles.

Includes: 

- bukuserver, a web frontend for [buku](https://github.com/jarun/Buku/)
- taskwarrior-web, a web frontend for [taskwarrior](https://taskwarrior.org)
